#!/usr/bin/env python3
import configparser
import json
import os
import shutil
import subprocess
import hashlib


def git_clone(key, repository):
    if not os.path.isdir('cache'):
        os.mkdir('cache')

    if not os.path.isdir('cache/{}'.format(key)):
        subprocess.run(['git', 'clone', repository, 'cache/{}'.format(key)])
    else:
        subprocess.run(['git', 'fetch'], cwd='cache/{}'.format(key))
        subprocess.run(['git', 'clean', '-fd'], cwd='cache/{}'.format(key))
        subprocess.run(['git', 'reset', '--hard'], cwd='cache/{}'.format(key))


def git_commit(key, commitmsg):
    subprocess.run(['git', 'add', '--all'], cwd='cache/{}'.format(key))
    subprocess.run(['git', 'commit', '-m', commitmsg], cwd='cache/{}'.format(key))


def get_git_hash(key, repository, tag):
    git_clone(key, repository)
    result = subprocess.check_output(['git', 'rev-list', '-n', '1', 'tags/{}'.format(tag)], cwd='cache/{}'.format(key),
                                     universal_newlines=True)
    return result.strip()


def get_current_rev():
    result = subprocess.check_output(['git', 'rev-list', '-n', '1', 'HEAD'], universal_newlines=True)
    return result.strip()


def sha256(filename):
    with open(filename, 'rb') as handle:
        input_bytes = handle.read()
    return hashlib.sha256(input_bytes).hexdigest()


print("This system exists only to work around how horrible flatpak is to work with")

config = configparser.ConfigParser()
config.read('actual-config.ini')

# Fun fact: flatpak doesn't even seem to know the version of my package
with open('../meson.build') as handle:
    raw = handle.readlines()
version = raw[1].split(': ')[1].replace('\'', "").replace(',', '').strip()
print("Going to release {}!".format(version))

print(" * Cleaning leftover fairy dust ", end='', flush=True)
if os.path.isdir('out'):
    shutil.rmtree('out')
print("[ok]")

print(" * Adding flathub boilerplate ", end='', flush=True)
os.mkdir('out')
shutil.copy2('template/flathub.json', 'out/flathub.json')
print("[ok]")

print(" * Reimplementing version control ", end='', flush=True)
with open('template/nl.brixit.wiremapper.json') as handle:
    flatpakfile = json.load(handle)

flatpakfile['runtime-version'] = config.get('settings', 'socially-acceptable-gnome-runtime')
# Yes I hardcoded the index here, don't @ me
flatpakfile['modules'][1]['sources'][0]['tag'] = config.get('settings', 'fairly-recent-libhandy-tag')
hash = get_git_hash('libhandy', flatpakfile['modules'][1]['sources'][0]['url'],
                    config.get('settings', 'fairly-recent-libhandy-tag'))
flatpakfile['modules'][1]['sources'][0]['commit'] = hash

print("[ok]")
print(" * Executing stupid flathub rules ", end='', flush=True)
current = get_current_rev()
for i, _ in enumerate(flatpakfile['modules'][3]['sources']):
    filename = flatpakfile['modules'][3]['sources'][i]['dest-filename']
    url = 'https://gitlab.com/MartijnBraam/wiremapper/-/raw/{}/data/{}'.format(current, filename)
    flatpakfile['modules'][3]['sources'][i]['url'] = url
    local_file = '../data/{}'.format(filename)
    flatpakfile['modules'][3]['sources'][i]['sha256'] = sha256(local_file)

with open('out/nl.brixit.wiremapper.json', 'w') as handle:
    json.dump(flatpakfile, handle)

print("[ok]")

print(" * Packaging all of python by myself ", end='', flush=True)
with open('template/python3-deps.json') as handle:
    wiremapper = json.load(handle)

# I really don't care about dep versions, the current ones work
if config.get('settings', 'pockethernet-ver') != '0.5':
    print("TODO: Implement this")
    exit(1)

with open('out/python3-deps.json', 'w') as handle:
    json.dump(wiremapper, handle)
print("[ok]")

print(" * Redoing more git work ", end='', flush=True)
with open('template/wiremapper.json') as handle:
    wiremapper = json.load(handle)

wiremapper['sources'][0]['tag'] = version
wiremapper['sources'][0]['commit'] = get_git_hash('wiremapper', wiremapper['sources'][0]['url'], version)

with open('out/wiremapper.json', 'w') as handle:
    json.dump(wiremapper, handle)
print("[ok]")

print(" * Conjuring the flatpak gods and request a fresh build ", end='', flush=True)
# Haha just kidding, push to flathub
git_clone('flathub', 'git@github.com:flathub/nl.brixit.wiremapper.git')
for fn in ['flathub.json', 'nl.brixit.wiremapper.json', 'python3-deps.json', 'wiremapper.json']:
    shutil.move('out/{}'.format(fn), 'cache/flathub/{}'.format(fn))
git_commit('flathub', "Release {}".format(version))
